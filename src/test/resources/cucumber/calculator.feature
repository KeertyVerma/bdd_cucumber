Feature: Calculator
  As a user
  I want to use a calculator
  So that I don't need to calculate myself

Scenario Outline: Substract two numbers in calculator
    Given I have a calculator
    When I substract <num1> and <num2>
    Then the result should be <total>
    
	Examples: 
    | num1 | num2 | total |
    | 12 | 3 | 9 |
    | 15 | 10 | 5 |